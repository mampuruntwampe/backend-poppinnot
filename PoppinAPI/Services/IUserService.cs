﻿using PoppinAPI.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PoppinAPI.Services
{
    public interface IUserService
    {
        IEnumerable<User> GetAll();
        User GetByID(int id);
        User Create(User us,string password);
        void Delete(int id);
        string Update(int id, User us);
        User Authenticate(string username,string password);


    }
}
