﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace PoppinAPI.Helpers
{
    public class ImageHandlerServices : IImageHandlerServices
    {
        private readonly IImageWriter _imageWriter;
        readonly PopDbContext _dbContext;

        public ImageHandlerServices(IImageWriter imageWriter,PopDbContext dbContext) {
            _imageWriter = imageWriter;
            _dbContext = dbContext;
        }

        public void SavePath(int id,string path)
        {
            var DbEvent = _dbContext.Event.Find(id);
            DbEvent.Image_Url = path;

            _dbContext.Event.Update(DbEvent);
            _dbContext.SaveChanges();
        }

        public async Task<string> UploadImage(IFormFile file)
        {
            var result = await _imageWriter.UploadImage(file);
            return result;
        }

    }
}
