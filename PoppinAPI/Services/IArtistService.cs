﻿using PoppinAPI.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PoppinAPI.Services
{
    interface IArtistService
    {
        IEnumerable<Artist> GetAll();
        Task GetByID(int id);
        Task Create(Artist card);
        Task Delete(int id);
        Task Update(int id, Artist card);
    }
}
