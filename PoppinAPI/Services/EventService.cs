﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PoppinAPI.Helpers;
using PoppinAPI.Models;

namespace PoppinAPI.Services
{
    public class EventService : IEventService
    {
        readonly PopDbContext _dbContext;

        public EventService(PopDbContext dbContext) {
            _dbContext = dbContext;
        }

        public Event Create(Event events)
        {
            if (_dbContext.Event.Any(x => x.Name == events.Name)) return null;

            _dbContext.Event.Add(events);
            _dbContext.SaveChanges();

            return events;
        }

        public void Delete(int id)
        {
            Event evens = _dbContext.Event.Find(id);
            if (evens != null)
            {
                _dbContext.Event.Remove(evens);
                _dbContext.SaveChanges();
            }
        }

        public IEnumerable<Event> GetAll()
        {
            return _dbContext.Event.ToArray();
        }

        public Event GetByID(int id)
        {
            return _dbContext.Event.Find(id);
        }

        public string Update(int id, Event events)
        {
            Event DbEvent = _dbContext.Event.Find(id);
            if (DbEvent == null) return null;

            DbEvent.Place = events.Place;
            DbEvent.Name = events.Name;
            DbEvent.Duration = events.Duration;
            DbEvent.Date = events.Date;
            
            _dbContext.Event.Update(DbEvent);
            _dbContext.SaveChanges();
            return "Successful";
        }
    }
}
