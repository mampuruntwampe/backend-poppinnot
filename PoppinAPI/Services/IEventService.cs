﻿using PoppinAPI.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PoppinAPI.Services
{
    public interface IEventService
    {
        IEnumerable<Event> GetAll();
        Event GetByID(int id);
        Event Create(Event evets);
        void Delete(int id);
        string Update(int id, Event evets);
    }
}
