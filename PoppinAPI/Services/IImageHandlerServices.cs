﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace PoppinAPI.Helpers
{
    public interface IImageHandlerServices
    {
        Task<string> UploadImage(IFormFile file);
        void SavePath(int id,string path);
    }
}
