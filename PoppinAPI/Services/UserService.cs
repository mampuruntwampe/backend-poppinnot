﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using PoppinAPI.Helpers;
using PoppinAPI.Models;

namespace PoppinAPI.Services
{
    public class UserService : IUserService
    {
        private PopDbContext _dbContext;
        readonly AppSettings _tokenKey;

        public UserService(PopDbContext dbContext,AppSettings tokenKey)
        {
            _dbContext = dbContext;
            _tokenKey = tokenKey;
        }

        public User Authenticate(string email,string password)
        {
            if (string.IsNullOrEmpty(email) || string.IsNullOrEmpty(password)) return null;
            var user = _dbContext.User.SingleOrDefault(x => x.Email == email);
            if (user == null) return null;

            if (!VerifyPasswordHash(password,user.Pass)) return null;

            return user;
        }

        public User Create(User Us,string password)
        {
            if (string.IsNullOrWhiteSpace(password)) return null;
            if (_dbContext.User.Any(x => x.Email == Us.Email)) return null;
   
            Us.Pass = CreatePassHash(password);

            _dbContext.User.Add(Us);
            _dbContext.SaveChanges();

            return Us;
        }

        public void Delete(int id)
        {
            User person = _dbContext.User.Find(id);
            if (person != null)
            {
                _dbContext.User.Remove(person);
                _dbContext.SaveChanges();
            }   
        }

        public IEnumerable<User> GetAll()
        {
            return _dbContext.User.ToArray();
        }

        public User GetByID(int id)
        { 
            return _dbContext.User.Find(id);
        }

        public string Update(int id, User user)
        {
            User DbUser = _dbContext.User.Find(id);
            if (DbUser == null) return null;

            if (user.Email != DbUser.Email)
            {
                if (_dbContext.User.Any(x => x.Email == user.Email)) return null;     
            }

            DbUser.Email = user.Email;
            DbUser.Cellnumber = user.Cellnumber;
            DbUser.Name = user.Name;
            DbUser.Surname = user.Surname;
            DbUser.Gender = user.Gender;

            _dbContext.User.Update(DbUser);
            _dbContext.SaveChanges();
            return "Updated Successful";
        }

        private static string CreatePassHash(string rawData)
        {
            // Create a SHA256   
            using (SHA256 sha256Hash = SHA256.Create())
            {
                // ComputeHash - returns byte array  
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(rawData));

                // Convert byte array to a string   
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }
                return builder.ToString();
            }
        }

        //Verify if password match 
        private static bool VerifyPasswordHash(string password,string HashedPassword)
        {
            if (password == null) throw new ArgumentNullException("password");
            if (string.IsNullOrWhiteSpace(password)) throw new ArgumentException("Value cannot be empty or whitespace only string.", "password");
            if (HashedPassword.Length != 64) throw new ArgumentException("Invalid length of password hash (64 bytes expected).", "passwordHash");

            if (CreatePassHash(password) != HashedPassword) return false;

            return true;
        }
    }
}
