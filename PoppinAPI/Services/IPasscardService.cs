﻿using PoppinAPI.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PoppinAPI.Services
{
    interface IPasscardService
    {
        IEnumerable<Passcard> GetAll();
        Task GetByID(int id);
        Task Create(Passcard card);
        Task Delete(int id);
        Task Update(int id, Passcard card);
    }
}
