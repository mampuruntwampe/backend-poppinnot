using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PoppinAPI.Models;

namespace PoppinAPI.Controllers
{
    [Route("api/artist")]
    public class ArtistController : Controller
    {

        private PopDbContext dbContext;

        public ArtistController() {
            this.dbContext = PopDbContextFactory.Create();
        }
        // GET api/values
        [HttpGet]
        public IEnumerable<Artist> GetAllArtists()
        {
            return this.dbContext.Artist.ToArray();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
