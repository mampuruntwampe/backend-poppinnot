using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PoppinAPI.Models;
using PoppinAPI.Services;

namespace PoppinAPI.Controllers
{
    [Route("api/[controller]")]
    public class EventController : Controller
    {
        readonly EventService _eventService;

        public EventController(EventService eventService){
            _eventService = eventService;
        }
       
        [HttpGet]
        public IEnumerable<Event> GetAllEvents()
        {
            return _eventService.GetAll();
        }

        [HttpPost("createEvent")]
        public IActionResult CreateEvent([FromBody] Event events)
        {
            if (events != null)
            {
                _eventService.Create(events);
                return Ok(events);
            }
            else
            {
                return BadRequest(new { message = "Could not add event"});
            }
        }

        [HttpDelete("delete/{id}")]
        public IActionResult DeleteEvent(int id)
        {
            _eventService.Delete(id);
            return Ok();
        }

        [HttpPut("update/{id}")]
        public IActionResult UpdateEvent(int id,[FromBody] Event events)
        {

            _eventService.Update(id, events);
            return Ok(events);
        }

        [HttpGet("event/{id}")]
        public IActionResult GetEventByID(int id)
        {
            Event events =  _eventService.GetByID(id);
            return Ok(events);
        }
    }
}
