﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PoppinAPI.Helpers;

namespace PoppinAPI.Controllers
{
    [Route("api/ImageUpload")]
    public class ImageUploadController : Controller
    {
        readonly IImageHandlerServices _imageHandler;

        public ImageUploadController(IImageHandlerServices imageHandler) {
            _imageHandler = imageHandler;
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UploadImage(int id,IFormFile file)
        {
            var path = await _imageHandler.UploadImage(file);
            _imageHandler.SavePath(id, path.ToString());
            return Ok();
                
        }
    }
}