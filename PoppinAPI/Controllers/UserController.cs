using System;
using Microsoft.AspNetCore.Mvc;
using PoppinAPI.Models;
using PoppinAPI.Services;
using PoppinAPI.Helpers;
using AutoMapper;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Extensions.Options;

namespace PoppinAPI.Controllers
{
    [Route("api/[controller]")]
    public class UserController : Controller
    {
        readonly UserService _userService;
        readonly PopAutoMapper _autoMapper;
        readonly IMapper _mapper;
        readonly AppSettings _tokenKey;

        public UserController(UserService userService,PopAutoMapper autoMapper,IMapper mapper,IOptions<AppSettings> tokenKey) {
            _userService = userService;
            _autoMapper = autoMapper;
            _mapper = mapper;
            _tokenKey = tokenKey.Value;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var users = _userService.GetAll();
            return Ok(users);
        }

        [HttpPost("Login")]
        public IActionResult Login([FromBody] User Us)
        {
            var user = _userService.Authenticate(Us.Email,Us.Pass);
            if (Us == null) return BadRequest(new { message = "Username or password is incorrect" });

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_tokenKey.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.User_ID.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(30),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);//checks if key is 128bits
            var tokenString = tokenHandler.WriteToken(token);

            return Ok(new
            {
                Id = user.User_ID,
                user.Name,
                FirstName = user.Surname,
                user.Email,
                Contact = user.Cellnumber,
                user.Gender,
                Token = tokenString
            });
        }

        [HttpPost("register")]
        public IActionResult Register([FromBody]User Us)
        {
            var user = _mapper.Map<User>(Us);

            if (Us != null)
            {
                _userService.Create(Us,user.Pass);
                return Ok(Us);
            }
            else
            {
                return BadRequest();
            }
        }

        [HttpPut("update/{id}")]
        public IActionResult UpdateUserDetails(int id, [FromBody]User Us)
        {
            var user = _mapper.Map<User>(Us);
            _userService.Update(id, Us);
            return Ok(user);
        }

        [HttpDelete("delete/{id}")]
        public IActionResult Delete(int id)
        {
            _userService.Delete(id);
            return Ok();
        }
    }
}
