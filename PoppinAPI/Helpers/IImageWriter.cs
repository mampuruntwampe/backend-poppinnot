﻿using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace PoppinAPI.Helpers
{
    public interface IImageWriter
    {
        Task<string> UploadImage(IFormFile file);
    }
}
