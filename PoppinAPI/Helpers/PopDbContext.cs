using Microsoft.EntityFrameworkCore;
using PoppinAPI.Models;

namespace PoppinAPI.Helpers
{
    public class PopDbContext : DbContext
    {
        public PopDbContext(DbContextOptions<PopDbContext> options)
        : base(options) { }

        public DbSet<Event> Event { get; set; }
        public DbSet<Artist> Artist { get; set; }
        public DbSet<User> User { get; set; }
        public DbSet<Passcard> Passcard { get; set; }

    }
}