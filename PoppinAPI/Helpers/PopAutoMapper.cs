﻿using AutoMapper;
using PoppinAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PoppinAPI.Helpers
{
    public class PopAutoMapper: Profile
    {
        public PopAutoMapper()
        {
            CreateMap<User, ResponseUser>();
            CreateMap<ResponseUser, User>();
        }
         
    }
}
