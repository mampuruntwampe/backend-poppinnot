using System.ComponentModel.DataAnnotations;

namespace PoppinAPI.Models {
    public class Passcard {
        [Key]
        public int Passcard_ID { get; set; }
        public string Varification { get; set; }
        public int Cost { get; set; }
        public string Level {get;set;}
        public int User_ID { get; set; }
        
    }
}