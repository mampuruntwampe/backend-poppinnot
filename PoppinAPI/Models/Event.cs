using System;
using System.ComponentModel.DataAnnotations;

namespace PoppinAPI.Models {
    public class Event {
        [Key]
        public int Event_ID { get; set; }
        public string Name { get; set; }
        public DateTime Date {get;set;}
        public int Duration {get;set;}
        public int Likes {get;set;}
        public int DisLikes {get;set;}
        public string Place {get;set;}
        public int Passcard_ID {get;set;}
        public string Image_Url { get; set; }
    }
}