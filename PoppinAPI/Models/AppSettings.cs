﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PoppinAPI.Models
{
    public class AppSettings
    {
        public string Secret { get; set; }
    }
}
