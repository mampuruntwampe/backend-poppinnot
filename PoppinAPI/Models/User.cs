using System.ComponentModel.DataAnnotations;

namespace PoppinAPI.Models {
    public class User {
        [Key]
        public int User_ID { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Gender {get;set;}
        public int Cellnumber {get;set;}
        public string Email {get;set;}
        public string Pass {get;set;}
        
    }
}