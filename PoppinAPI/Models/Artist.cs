using System;
using System.ComponentModel.DataAnnotations;

namespace PoppinAPI.Models {
    public class Artist {
        [Key]
        public int Artist_ID { get; set; }
        public string Name { get; set; }
        public int Duration {get;set;}
        public int Rating {get;set;}
        public int Event_ID {get;set;}
    }
}