 CREATE TABLE `popping_not`.`user` (
    `user_id` INT AUTO_INCREMENT NOT NULL,
    `name` VARCHAR(45) NOT NULL,
    `surname` VARCHAR(45) NOT NULL,
    `gender` VARCHAR(45) NOT NULL,
    `cellnumber` INT(10) NOT NULL,
    `email` VARCHAR(45) NULL,
    `pass` VARCHAR(256) NOT NULL,
    PRIMARY KEY (`user_id`)
)  ENGINE=INNODB;
 
CREATE TABLE `popping_not`.`passcard` (
    `passcard_id` INT AUTO_INCREMENT NOT NULL,
    `varification` VARCHAR(200) NOT NULL,
    `cost` INT NOT NULL,
    `level` VARCHAR(10) NOT NULL,
    `user_id` INT NOT NULL,
    PRIMARY KEY (`passcard_id`),
    FOREIGN KEY (user_id)
        REFERENCES user (user_id)
        ON UPDATE CASCADE ON DELETE RESTRICT
)  ENGINE=INNODB;
 
CREATE TABLE `popping_not`.`event` (
    `event_id` INT AUTO_INCREMENT NOT NULL,
    `name` VARCHAR(45) NOT NULL,
    `date` DATETIME NOT NULL,
    `duration` VARCHAR(45) NOT NULL,
    `likes` INT(100) NULL,
    `dislikes` INT(100) NULL,
    `place` VARCHAR(45) NOT NULL,
    `passcard_id` INT NOT NULL,
    `image_url` INT NOT NULL,
    PRIMARY KEY (`event_id`),
    FOREIGN KEY (passcard_id)
        REFERENCES passcard (passcard_id)
        ON UPDATE CASCADE ON DELETE RESTRICT
)  ENGINE=INNODB;
  
 
CREATE TABLE `popping_not`.`artist` (
    `artist_id` INT AUTO_INCREMENT NOT NULL,
    `name` VARCHAR(45) NOT NULL,
    `duration` INT NOT NULL,
    `rating` INT NULL,
    `event_id` INT NOT NULL,
    PRIMARY KEY (`artist_id`),
    FOREIGN KEY (event_id)
        REFERENCES event (event_id)
        ON UPDATE CASCADE ON DELETE RESTRICT
)  ENGINE=INNODB;
  
  